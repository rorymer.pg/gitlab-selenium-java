package com.obsidian;

import static org.junit.Assert.assertEquals;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
import java.net.MalformedURLException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ObsidianTest  {
    private WebDriver driver;

@Before 
public void setUp(){
    WebDriverManager.chromedriver().setup();
    ChromeOptions options = new ChromeOptions();  
    options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors"); 
    try {
        driver = new RemoteWebDriver(new URL("http://selenium__standalone-chrome:4444/wd/hub/"), options);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    } catch(MalformedURLException ex){}
}

@After
public void teardown() {
    if (driver != null) {
        driver.quit();
    }
}

    @Test
	public void whatsNew(){
		
		driver.get("https://www.obsidianqa.com");
		driver.findElement(By.linkText("What\'s New")).click();

        WebElement whatsNewDisplay = driver.findElement(By.cssSelector("body > div.hero-default.newhero > div.container > div > h1"));
        String result = whatsNewDisplay.getText();
        assertEquals(result, "What's New");
	}
}
